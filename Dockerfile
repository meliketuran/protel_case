from python:3-alpine 

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .

ENTRYPOINT ["gunicorn", "app:app" , "-b=0.0.0.0:8000" , "-w=3"] 